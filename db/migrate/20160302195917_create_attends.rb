class CreateAttends < ActiveRecord::Migration
  def change
    create_table :attends do |t|
      t.text :check_in
      t.text :check_out
      t.text :total_hours
      t.references :user, index: true, foreign_key: true
      add_foreign_key :attends, :users
      add_index	:attends, [ :user_id, :created_at ]

      t.timestamps null: false
    end
  end
end
