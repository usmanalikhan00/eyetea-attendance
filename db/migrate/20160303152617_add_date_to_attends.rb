class AddDateToAttends < ActiveRecord::Migration
  def change
    add_column :attends, :date, :datetime
  end
end
