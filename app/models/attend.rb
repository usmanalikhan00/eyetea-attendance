class Attend < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  validates :check_in, presence: true
  validates :check_out, presence: true
end
