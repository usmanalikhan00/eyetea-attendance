module UsersHelper
  
  def check_if_date_matches(check_date, current_date)
    flag = false
    check_date.each do |date|
      if current_date == date.created_at.to_date
        flag = true
      end
    end

    return flag
  end
end

