class UsersController < ApplicationController
	include UsersHelper
	protect_from_forgery
	def login
		@user ||= User.find_by(email: params['email'], password: params["password"])
		if @user

			render json: { status: :true, user: @user } 
		else
	    render json: { status: :false, response: 'NO SUCH USER' } 
		end
	end
	
	def add_attend
		@user = User.find(attend_params["id"])
		attend_params_without_id = attend_params.except(:id)
		if @user
			@check_times = @user.attends.build(attend_params)
			check_date = @user.attends.all
			current_date = attend_params['date'].to_date
			result = check_if_date_matches(check_date, current_date)

			if ((@check_times.check_in > @check_times.check_out || @check_times.check_in == @check_times.check_out ) || result==true)
				render json: {status: :false, response: 'Check In/Check Out is Incorrect OR Attendance Already Posted.'}
			else
				@attend = @user.attends.create!(attend_params_without_id)
				@all_attends = Attend.all
				render json: { status: :true, user: @user, attend: @attend, all_attends: @all_attends }
		 	end
		else
			render json: { status: :false, response: 'NO SUCH USER' } 
		end 
	end

	def register
		@user = User.new(register_params)
		if @user.save
			 render json: { status: :true, user: @user } 
		else
			 render json: { status: :false, error: @user.errors.full_messages, response: 'There is an Error while creating User.' } 
		end 
	end
	def allusers
		@users = User.all
		if @users
			 render json: { status: :true, user: @users } 
		else
			 render json: { status: :false, response: 'There are no Users in the System' } 
		end 
	end
	
	private
  	def attend_params
  		params.require(:attend).permit(:id, :check_in, :check_out, :total_hours, :date)	
  	end
    # Use callbacks to share common setup or constraints between actions.
    def register_params
      	params.require(:user).permit(:name, :email, :password)
    end

end
