class UsersController < ApplicationController
	protect_from_forgery
	
	def login
		@user ||= User.find_by(email: params['email'], password: params["password"])
		if @user
			respond_to do |format|  
			    format.json { render json: { status: :true, user: @user } }
			    # format.html 
			    ## Other format
			end      
		else
			respond_to do |format| 
			    format.json { render json: { status: :false, response: 'NO SUCH USER' } }
			    # format.html 
			    ## Other format
			end      
		end
	end

	def add_attend
		@user = User.find(params["id"])
		if @user
			@attend = @user.attends.create!(check_in: "45", check_out: "70", total_hours: "25", date: "21/3/2016")
			@all_attends = @user.attends.all
			respond_to do |format|
			    format.json { render json: { status: :true, user: @user, attend: @attend, all_attends: @all_attends } }
			    # format.html 
			    ## Other format
			end      
		else
			respond_to do |format|
			    format.json { render json: { status: :false, response: 'NO SUCH USER' } }
			    # format.html 
			    ## Other format
			end      
		end 
	end
end
